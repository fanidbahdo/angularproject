import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../..', 'Backend'))
from app import Item


def test_new_item():
    """
    GIVEN a User model
    WHEN a new User is created
    THEN check the email, hashed_password, and role fields are defined correctly
    """
    item = Item('patkennedy79@gmail.com', 'FlaskIsAwesome')
    assert item.name == 'patkennedy79@gmail.com'
    assert item.surname != 'FlaskIsAjhgwesome'
