from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask import render_template, request, redirect, url_for
from flask import jsonify
from flask_cors import CORS

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:root@localhost/flask_db'
app.debug = True
db = SQLAlchemy(app)
CORS(app)

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)

    def __init__(self, username, email):
        self.username = username
        self.email = email
    
    def __repr__(self):
        return '<User %r>' % self.username

class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), unique=True)
    surname = db.Column(db.String(120), unique=True)

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname
    
    def serialize(self):
        return {"name": self.name,
                "surname": self.surname}

@app.route('/')
def index():
    myUser = User.query.all()
    oneItem = User.query.filter_by(username='Fatima').first()
    return render_template("add_user.html", myUser=myUser, oneItem=oneItem)

@app.route('/items')
def get_items():
    items = Item.query.all()
    it = []
    for item in items:
        it.append(item.serialize())
    return jsonify(it)

#DYNAMIC URL
@app.route('/profile/<username>')
def profile(username):
    user = User.query.filter_by(username=username).first()
    return render_template("profile.html", user=user)

# POST DATA TO DATABASE
@app.route('/post_user', methods=['POST'])
def post_user():
    user = User(request.form['username'], request.form['email'])
    db.session.add(user)
    db.session.commit()
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run()




# from flask import Flask, render_template
# from flask_sqlalchemy import SQLAlchemy
# from flask_security import Security, SQLAlchemyUserDatastore, \
#     UserMixin, RoleMixin, login_required

# # Create app
# app = Flask(__name__)
# app.config['DEBUG'] = True
# app.config['SECRET_KEY'] = 'super-secret'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'

# # Create database connection object
# db = SQLAlchemy(app)

# # Define models
# roles_users = db.Table('roles_users',
#         db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
#         db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

# class Role(db.Model, RoleMixin):
#     id = db.Column(db.Integer(), primary_key=True)
#     name = db.Column(db.String(80), unique=True)
#     description = db.Column(db.String(255))

# class User(db.Model, UserMixin):
#     id = db.Column(db.Integer, primary_key=True)
#     email = db.Column(db.String(255), unique=True)
#     password = db.Column(db.String(255))
#     active = db.Column(db.Boolean())
#     confirmed_at = db.Column(db.DateTime())
#     roles = db.relationship('Role', secondary=roles_users,
#                             backref=db.backref('users', lazy='dynamic'))

# # Setup Flask-Security
# user_datastore = SQLAlchemyUserDatastore(db, User, Role)
# security = Security(app, user_datastore)

# # Create a user to test with
# @app.before_first_request
# def create_user():
#     db.create_all()
#     user_datastore.create_user(email='matt@nobien.net', password='password')
#     db.session.commit()

# # Views
# @app.route('/')
# @login_required
# def home():
#     return render_template('index.html')

# if __name__ == '__main__':
#     app.run()